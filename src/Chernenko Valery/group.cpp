#include "student.h"
#include "group.h"
#include <iostream>
#include <time.h>

	//����� ���������� � ������;
	ostream& operator<<(ostream& os, const Group& group)
	{
		os << "������ " << group.title << ":" << endl;
		if (group.head)
			os << "�������� :" << *group.head << endl;
		else
			os << "�������� �� ��������!" << endl;
		os << "������ ���������: " << endl;
		for (int i = 0; i < group.num; i++)
		{
			os << *group.st[i];
			os << " ������� ���� ��������: " << group.st[i]->getAvMark() << endl;
		}
		return os;
	}

	//������������;
	Group::Group(string title)
	{
		this->title = title;
		head = nullptr;
		st = nullptr;
		num = 0;
	}

	Group::Group(Group& gr)
	{
		title = gr.title;
		head = gr.head;
		num = gr.num;
		if (num)
		{
			st = new Student*[num];
			for (int i = 0; i < num; i++)
				st[i] = gr.st[i];
		}
		else
			st = nullptr;
	}

	//����������;
	Group::~Group()
	{
		for (int i = 0; i < num; i++)
			st[i]->setGroup(nullptr);
		delete[] st;
	}

	//�������;
	void Group::setHead(Student* st) { head = st; }

	//�������;
	Student* Group::getHead() { return head; }
	string Group::getTitle() { return title; }
	Student* Group::getRandStud()
	{
		#ifndef _STUDRAND_
		#define _STUDRAND_
		srand((unsigned int)time);
		#endif
		return st[rand() % num];
	}

	//���������� ��������;
	void Group::addStud(Student* stud)
	{
		if (num == 0)
		{
			st = new Student*[num+1];
			st[num] = stud;
		}
		else
		{
			Student** tmp = new Student*[num];
			for(int i = 0; i < num; i++)
				tmp[i] = st[i];
//			for (int i = 0; i < num; i++)
//				delete st[i];
			delete[] st;
			st = new Student*[num+1];
			for (int i = 0; i < num ; i++)
			{
				st[i] = tmp[i];
			}
//			for (int i = 0; i < num; i++)
//				delete tmp[i];
			delete[] tmp;
			st[num] = stud;
		}
		st[num]->setGroup(this);
		num++;
	}

	//����� �������� � ������ �� id;
	Student* Group::findStudId(int id)
	{
		for (int i = 0; i < num; i++)
			if (st[i] -> getID() == id)
				return st[i];
		return nullptr;
	}

	//����� �������� �� �������;
	Student* Group::findStudFio(string fio)
	{
		for (int i = 0; i < num; i++)
			if (st[i] -> getFIO() == fio)
				return st[i];
		return nullptr;
	}

	//�������� �������� �� id;
	int Group::removeStud(int id)
	{
		int i;
		int count;
		for(i = 0; i < num && st[i] -> getID() != id; i++);
		if (i == num)
		{
			cout << "��������� ������� � ������ �� ������!" << endl;
			return 0;
		}
		else
		{
			st[i] -> setGroup(nullptr);
			if (head -> getID() == st[i] -> getID())
			{
				head = nullptr;
				count = 2;
			}
			else 
				count = 1;
			for(int j = i; j < num-1; j++)
				st[j] = st[j+1];
			Student** tmp = new Student*[num - 1];
			for(int j = 0; j < num-1; j++)
				tmp[j] = st[j];
			delete[] st;
			st = new Student*[num - 1];
			for (int j = 0; j < num; j++)
				st[j] = tmp[j];
			delete[] tmp;
			num--;
		}
		return count;
	}

	//��������� ������� ������ �� ������;
	double Group::getAvMark()
	{
		if (num==0)
			return 0;
		else 
		{
			double sum = 0;
			int corNum = 0;
			for (int i = 0; i < num; i++)
			{
				double count = st[i] -> getAvMark();
				if (count)
				{
					corNum++;
					sum+=count;
				}
			}
			if (corNum == 0)
				return 0;
			return sum/corNum;
		}
	}