#include "student.h"
#include "group.h"

	ostream& operator<<(ostream& os, const Student& student)
	{
		os << "Id: " << student.id << " Fio: " << student.fio << " ";
		os << "���������� ������: " << student.num << " ";
		os << "������: ";
		for (int i = 0; i < student.num; i++)
			os << student.marks[i];
		return os;
	}
	//������������;
	Student::Student(int id, string fio)
	{
		this->id = id;
		this->fio = fio;
		marks = nullptr;
		num = 0;
		gr = nullptr;
	}

	Student::Student(Student& st)
	{
		id = st.id;
		fio = st.fio;
		num = st.num;
		if (num)
		{
			marks = new int[num];
			for (int i = 0; i<num; i++)
				marks[i] = st.marks[i];
		}
		else
			marks = nullptr;
		gr = st.gr;
	}

	//����������;
	Student::~Student()
	{	
		delete[] marks;
	}

	//�������;
	void Student::setGroup(Group* grp) {gr = grp;}
	
	//�������;
	Group* Student::getGroup() {return gr;}
	string Student::getFIO() {return fio;}
	int Student::getID() {return id;}
	int Student::getNumMarks() {return num;}
	int* Student::getMarks() {return marks;}
	
	//���������� ������;
	void Student::addMark(int n)
	{
		if (num==0)
		{
			marks = new int[num+1];
			marks[num] = n;
		}
		else
		{
			int* tmp = new int[num+1];
			for(int i = 0; i < num; i++)
				tmp[i] = marks[i];
			tmp[num] = n;
			delete[] marks;
			marks = new int[num + 1];
			for (int i = 0; i < num + 1; i++)
				marks[i] = tmp[i];
			delete[] tmp;
		}
		num++;
	}
	
	//��������� ������� ������;
	double Student::getAvMark()
	{
		double sum = 0;
		if (num == 0)
			return 0;
		else
		{
			for(int i = 0; i < num; i++)
				sum += (double)marks[i];
			return sum/num;
		}
	}
	
	//��������� ����������� ������;
	int Student::getMinMarks()
	{
		int min = 6;
		for (int i = 0; i < num; i++)
			if (marks[i] < min)
				min = marks[i];
		if (min == 6) 
			return 0;
		else return min;
	}
	
	//��������� ���������� ������������ ������;
	int Student::howMarks(int mark)
	{
		int count = 0;
		for (int i = 0; i < num; i++)
			if (marks[i] == mark)
				count ++;
		return count;
	}