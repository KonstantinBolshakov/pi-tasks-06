﻿#include "Deanery.h"
#include <string>
#include <iostream>
int main()
{
	setlocale(LC_ALL, "rus");
	Deanery dean;
	dean.loadGroups("groups.txt");
	dean.loadStudent("student.txt");
	//Узнать, в какой группе некоторый студент
	int id = 14;
	cout<< dean.findGroup(id)<<endl;
	//Найти отличников в некоторой группе
	int TitleGr = 381608;
	dean.findExcellentSt(TitleGr);
	//вывести срений балл групп
	dean.PrintAvMarkGroup();
	//Находим лучшего студента
	dean.findBestStudent();
	//Находим лучшую группу
	dean.findBestGroup();
	//Вывод информации о студенте
	dean.PrintStudent(id);
	//Выбираем старост в группы
	dean.setHead();
	//Вывести старосту некоторой группы
	dean.PrintHead(TitleGr);
	//Добавляем случайную оценку некоторому студенту
	dean.AddRandMark(id);
	//Удаляем студента за неуспеваемость
    dean.DeleteBadStudent();
	//Переводим студента (кого, куда)
	dean.TransferStudent(id,TitleGr);
	//Сохранение данных в файле
	dean.SaveFile("student.txt");
	
	return 0;
}