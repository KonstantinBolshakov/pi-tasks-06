#include <iostream>
#include <string>
#include "Student.h"
#include "Group.h"

using namespace std;

Group::Group()
{
	title = "-";
	std = 0;
	number = 0;
	head = 0;
}

Group::Group(string title) 
{
	this->title = title;
	std = 0;
	number = 0;
	head = 0;
}

Group::~Group() {
	delete[] std;
	delete[] head;
}

void Group::addStudent(Student *s)
{
	if (number == 0)
	{
		std = new Student *[1];
		std[number++] = s;
	}
	else
	{
		Student **tmp = new Student *[number + 1];
		for (int i = 0; i < number; i++)
			tmp[i] = std[i];
		delete[] std;
		std = tmp;
		std[number++] = s;
	}
	s->toGroup(this);
}

bool Group::setRandHead()
{
	if (number != 0)
	{
		head = std[rand() % number];
		return true;
	}
	return false;
}

bool Group::searchStudent(string fio)
{
	for (int i = 0; i < number; i++)
		if (std[i]->getFio() == fio)
			return true;
	return false;
}

bool Group::searchStudent(int id)
{
	for (int i = 0; i < number; i++)
		if (std[i]->getId() == id)
			return true;
	return false;
}

float Group::averageGrMark() 
{
	float sum = 0;
	if (number != 0)
	{
		for (int i = 0; i < number; i++)
			sum += std[i]->averageMark();
		return sum / (float)number;
	}
	return sum;
}

bool Group::delStudent(int id)
{
	for (int i = 0; i < number; i++)
		if (std[i]->getId() == id)
		{
			if (head = std[i]) head = 0;
			std[i] = std[number-- - 1];
			return true;
		}
	return false;
}

void Group::info() 
{
	cout << "Title: " << title << endl;
	cout << "Average mark: " << averageGrMark() << endl;
	if (head != 0) cout << "Head: " << head->getFio() << endl;
	cout << number << " students: " << endl;
	for (int i = 0; i < number; i++)
		cout << "ID: " << std[i]->getId() << " Name of student: " << std[i]->getFio() << endl;
	cout << endl;
}

string Group::getTitle() const 
{ 
	return title;
}