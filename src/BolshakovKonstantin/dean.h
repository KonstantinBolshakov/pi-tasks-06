#pragma once

#include <string>
#include "Student.h"
#include "Group.h"

using namespace std;

class dean {
private:
	Student **students;
	Group **groups;
	int numbStd;
	int numbGrp;
public:
	dean();
	~dean();

	void readGroup(string file);
	bool groupsInfo();
	void readStudents(string file);
	bool studentsInfo();
	void writeStudents(string file);
	Group* searchGroup(string title);
	void randomMarks(int number);
	bool transferStudent(int id, string groupName);
	void statistic();
	bool delStudent(int id);
	void dismissalStudents(float min);
	void setRandHeads();
};

void separateString(string str, int &id, string &fio, string &groupName);