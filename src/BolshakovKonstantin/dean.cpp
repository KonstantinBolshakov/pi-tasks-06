﻿#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include "Student.h"
#include "Group.h"
#include "dean.h"

using namespace std;

dean::dean() {
	students = 0;
	groups = 0;
	numbStd = 0;
	numbGrp = 0;
}

dean::~dean() {
	delete[] students;
	delete[] groups;
}

void dean::readGroup(string file) 
{
	string groupName;
	int i = 0;
	ifstream f(file);
	while (getline(f, groupName)) 
	{
		numbGrp++;
	}
	f.clear();
	f.seekg(0);
	groups = new Group *[numbGrp];
	while (getline(f, groupName)) 
	{
		groups[i++] = new Group(groupName);
	}
	f.close();
}

bool dean::groupsInfo()
{
	if (numbGrp == 0)
	{
		cout << "No any groups." << endl;
		return false;
	}
	else
	{
		for (int i = 0; i < numbGrp; i++)
			groups[i]->info();
		return true;

	}
}

void dean::readStudents(string file)
{
	int i = 0;
	int id = 0;
	string fio;
	string grName;
	string stFio;
	ifstream f(file);
	while (getline(f, stFio))
	{
		numbStd++;
	}
	f.clear();
	f.seekg(0);
	students = new Student *[numbStd];
	while (getline(f, stFio)) {
		separateString(stFio, id, fio, grName);
		students[i] = new Student(id, fio);
		searchGroup(grName)->addStudent(students[i++]);
		id = 0;
	}
	f.close();
}

bool dean::studentsInfo() 
{
	if (numbStd == 0)
	{
		cout << "No any students." << endl;
		return false;
	}
	else
	{
		for (int i = 0; i < numbStd; i++)
			students[i]->info();
		return true;

	}
}

void dean::writeStudents(string file) 
{
	ofstream f(file);
	for (int i = 0; i < numbStd; i++)
		f << students[i]->getId() << " " << students[i]->getFio() << " " << students[i]->getGroup()->getTitle() << endl;
	f.close();
}

Group* dean::searchGroup(string title) 
{
	for (int i = 0; i < numbGrp; i++)
		if (groups[i]->getTitle() == title)
			return groups[i];
	return 0;
}

void dean::randomMarks(int number)
{
	for (int i = 0; i < numbStd; i++)
		for (int j = 0; j < number; j++)
			students[i]->addMark(rand() % 5 + 1);
}

bool dean::transferStudent(int id, string groupName) 
{
	for (int i = 0; i < numbStd; i++)
		if (students[i]->getId() == id)
		{
			students[i]->getGroup()->delStudent(id);
			for (int j = 0; j < numbGrp; j++)
				if (groups[j]->getTitle() == groupName)
				{
					groups[j]->addStudent(students[i]);
					return true;
				}
		}
	return false;
}

void dean::statistic() 
{
	float sum = 0;
	float av = 0;
	int max = 0;
	int maxInd = 0;
	for (int i = 0; i < numbGrp; i++)
	{
		av = groups[i]->averageGrMark();
		sum += av;
		if (av > max)
		{
			max = av;
			maxInd = i;
		}
	}
	cout << "Statistics: " << endl;
	cout << "Average mark of groups: " << sum / numbGrp << endl;
	cout << "Best group: " << groups[maxInd]->getTitle() << endl;
	max = 0;
	maxInd = 0;
	for (int j = 0; j < numbStd; j++)
		if (students[j]->averageMark() > max)
		{
			max = students[j]->averageMark();
			maxInd = j;
		}
	cout << "Best student: " << endl;
	students[maxInd]->info();
}

bool dean::delStudent(int id) 
{
	for (int i = 0; i < numbStd; i++)
		if (students[i]->getId() == id)
		{
			students[i] = students[numbStd-- - 1];
			return true;
		}
	return false;
}

void dean::dismissalStudents(float min)
{
	for (int i = 0; i < numbStd; i++)
		if (students[i]->averageMark() < min)
		{
			students[i]->getGroup()->delStudent(students[i]->getId());
			delStudent(students[i]->getId());
		}
}

void dean::setRandHeads()
{
	for (int i = 0; i < numbGrp; i++)
		groups[i]->setRandHead();
}

void separateString(string str, int &id, string &fio, string &groupName)// промежуток между блоками 
{ 
	int i = 0;
	while (str[i] != ' ') id = (id * 10) + (str[i++] - '0');
	int bgnFio = i + 1;
	while (str[i] < '0' || str[i] > '9') i++;
	fio = str.substr(bgnFio, i - 1 - bgnFio);
	groupName = str.substr(i);
}