#include <iostream>
#include <string>

using namespace std;

#include "Group.h"
#include "Student.h"

Student::Student() {
	id = 0;
	fio = "-";
	grp = 0;
	marks = 0;
	number = 0;
}

Student::Student(int id, string fio) {
	this->id = id;
	this->fio = fio;
	grp = 0;
	marks = 0;
	number = 0;
}

Student::~Student() {
	delete[] marks;
}

int Student::getId() const 
{ 
	return id;
}

string Student::getFio() const 
{ 
	return fio;
}

Group* Student::getGroup() const 
{ 
	return grp;
}

void Student::info() 
{
	cout << "ID: " << id << endl;
	cout << "Student's name is " << fio << endl;
	cout << "Group is " << grp->getTitle() << endl;
	cout << "Number of marks are " << number << endl;
	cout << "Marks are ";
	for (int i = 0; i < number; i++)
		cout << marks[i] << " | ";
	cout << endl << "Average mark is " << averageMark() << endl << endl;
}

float Student::averageMark() {
	int sum = 0;
	if (number != 0)
	{
		for (int i = 0; i < number; i++)
			sum += marks[i];
		return sum / (float)number;
	}
	return sum;
}

void Student::toGroup(Group *gr)
{
	this->grp = gr;
}

void Student::addMark(int m)
{
	if (number == 0)
	{
		marks = new int[1];
		marks[number++] = m;
	}
	else
	{
		int *tmp = new int[number + 1];
		for (int i = 0; i < number; i++)
			tmp[i] = marks[i];
		tmp[number++] = m;
		delete[] marks;
		marks = tmp;
	}
}

