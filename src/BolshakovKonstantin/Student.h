#pragma once

#include <string>
#include "Group.h"

using namespace std;

class Group;

class Student {
private:
	int id;
	string fio;
	Group *grp;
	int *marks;
	int number;
public:
	Student();
	Student(int id, string fio);
	~Student();

	int getId() const;
	string getFio() const;
	Group* getGroup() const;
	void info();
	void addMark(int m);
	void toGroup(Group *st);
	float averageMark();
};