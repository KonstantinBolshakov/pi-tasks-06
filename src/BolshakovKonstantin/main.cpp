#include <iostream>
#include <string>
#include "Student.h"
#include "Group.h"
#include "Dean.h"

using namespace std;



int main()
{
	dean dekan;
	dekan.readGroup("group.txt");
	dekan.readStudents("student.txt");
	dekan.randomMarks(10);		
	dekan.setRandHeads();		
	dekan.groupsInfo();			
	dekan.transferStudent(12, "31603");		//������� �������� � ID 43 � ������ "381603"
	dekan.dismissalStudents(2.1);		//���������� ���������, � ������� ������� ���� ������ 2.5
	dekan.groupsInfo();
	dekan.statistic();			
	dekan.writeStudents("new_student.txt");
	return 0;
}